
<div class="col-md-4 navbar-default nav">
		<ul class="navbar-default nav" style="height: 100vh">
				<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
				<li><a href="#new-items" data-toggle="collapse"><i class="fa fa-list fa-fw"></i> Posts <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>

				<ul class="nav collapse" id="new-items">
					<li><a href=" {{ url('admin/posts') }} "><div class="col-sm-1"></div><i class="fa fa-list-alt fa-fw"></i> All Posts</a></li>
					<li><a href=" {{ url('admin/posts/create') }} "><div class="col-sm-1"></div><i class="fa fa-edit fa-fw"></i> Create Post</a></li>
				</ul>
				</li>
				<li><a href="#new-cats" data-toggle="collapse"><i class="fa fa-edit fa-fw"></i> Categories <span class="pull-right"><i class="fa fa-angle-down"></i></a>
				<ul class="nav collapse" id="new-cats">
					<li><a href="{{ url('admin/categories') }}"><div class="col-sm-1"></div><i class="fa fa-list-alt fa-fw"></i> All Categories</a></li>
					<li><a href="{{ url('admin/categories/create') }}"><div class="col-sm-1"></div><i class="fa fa-edit fa-fw"></i> Create Category</a></li>
				</ul>
				</li>
				<li><a href="#new-tags" data-toggle="collapse"><i class="fa fa-tags fa-fw"></i> Tags <span class="pull-right"><i class="fa fa-angle-down"></i></a>
				<ul class="nav collapse" id="new-tags">
					<li><a href="{{ url('admin/tags') }}"><div class="col-sm-1"></div><i class="fa fa-list-alt fa-fw"></i> All Tags</a></li>
					<li><a href="{{ url('admin/tags/create') }}"><div class="col-sm-1"></div><i class="fa fa-edit fa-fw"></i> Create Tag</a></li>
				</ul>
				</li>
				<li><a href="{{ url('admin/users') }}"><i class="fa fa-users fa-fw"></i> Users</a></li>
				<li><a href="{{ url('admin/comments') }}"><i class="fa fa-comments fa-fw"></i> Comments</a></li>
				<li><a href="{{ url('admin/settings') }}"><i class="fa fa-gear fa-fw"></i> Site Settings</a></li>
		</ul>
	</div>
