@extends('master')

@section('title') My Blog @endsection

@section('content')

@foreach ($posts as $post)
<article>                <!-- First Blog Post -->
    <img class="img-responsive" src="uploads/images/{{ $post->image }}" alt="">
        <h2><a href="post/{{ $post->slug  }} ">{{ $post->title }}</a></h2>
        <p><i class="fa fa-clock-o"></i> Posted on {{ Carbon\Carbon::parse($post->created_at)->format('d-m-Y') }} &nbsp; &nbsp; <i class="fa fa-user"></i> {{ $post->user->name }} &nbsp; &nbsp; @if(isset($post->category->name)) <i class="fa fa-tag"></i> {{ $post->category->name }} @endif   </p>

        <p> {!! substr((strip_tags($post->body)), 0,500) !!}...  </p>
            <a class="btn btn-primary" href="post/{{ $post->slug  }} ">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
    <hr>
</article>
@endforeach

{{ $posts->links('vendor.pagination.custom') }} 


@endsection

