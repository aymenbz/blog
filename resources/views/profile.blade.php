@extends('master')

@section('title') Account Setting @endsection

@section('content')

<div class="profile">
<h1 class="text-center">Account Setting</h1>
 @if (Session::has('success'))
<div class="alert alert-success">{!! Session::get('success') !!}</div>
@endif
@if (Session::has('failure'))
    <div class="alert alert-danger">{!! Session::get('failure') !!}</div>
@endif
<form class="form-horizontal" action="/profile/{{$user->id}}/update" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="control-label col-sm-2">Name</label>
            <div class="col-sm-7">
                <input type="text" name="name" class="form-control" value="{{ $user->name }}"></input>
            </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Email</label>
            <div class="col-sm-7">
                <input type="text" name="email" class="form-control" value="{{ $user->email }}"></input>
            </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Avatar</label>
            <div class="col-sm-7">
                <input type="file" name="avatar" class="form-control"></input>
            </div>
    </div>
    <div class="form-group">
            <div class="col-sm-7 col-sm-offset-2">
                <input type="submit" class="btn btn-primary btn-block" value="Update"></input>
            </div>
    </div>
</form>
@foreach ($errors->all() as $error)
<ul class="list-unstyled">
<div class="alert alert-danger"><li> {{ $error }} </li></div>
</ul>
@endforeach

</div>

@endsection