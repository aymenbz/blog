@extends('admin.master')

@section('title') Comments @endsection

@section('content') 

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>All Comments</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href=" {{ url('/admin/comments') }} ">Comments</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<table class="table text-center table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>body</th>
      <th>Author</th>
      <th>Posted At</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($comments as $comment)
    <tr>
      <th>{{$comment->id}}</th>
      <th><?php echo substr($comment->body, 0, 22) ?> ... </th>
      <th>{{ $comment->user->name }}</th>
      <th>{{ $comment->created_at }}</th>
      <th>
        <div class="btn-group" role="group" aria-label="...">
         <a href="comments/delete/{{ $comment->id }} " class="btn btn-danger confirm"><i class="fa fa-trash fa-fw"></i> Delete</a>
        </div>
      </th>
    </tr>
      @endforeach
  </tbody>
</table>
 {{ $comments->links('vendor.pagination.custom') }}
@endsection