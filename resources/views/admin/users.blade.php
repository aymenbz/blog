@extends('admin.master')

@section('title') Users @endsection

@section('content') 

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>All Users</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href=" {{ url('/admin/users') }} ">Users</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<table class="table text-center table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Email</th>
      <th>Register At</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($users as $user)
    <tr>
      <th>{{$user->id}}</th>
      <th>{{$user->name}} </th>
      <th>{{ $user->email }}</th>
      <th>{{ $user->created_at }}</th>
      <th>
        <div class="btn-group" role="group" aria-label="...">
         <a href="users/delete/{{ $user->id }} " class="btn btn-danger confirm"><i class="fa fa-trash fa-fw"></i> Delete</a>
        </div>
      </th>
    </tr>
      @endforeach
  </tbody>
</table>
 {{ $users->links('vendor.pagination.custom') }}
@endsection