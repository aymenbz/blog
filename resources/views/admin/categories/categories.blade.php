@extends('admin.master')

@section('title') Categories @endsection

@section('content') 

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>All Categories</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href=" {{ url('/admin/categories') }} ">Categories</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<table class="table text-center table-bordered">
  <thead>
    <tr>
      <td>ID</td>
      <td>Name</td>
      <td>Description</td>
      <td>Action</td>
    </tr>
  </thead>
  <tbody>
  @foreach ($cats as $cat)
    <tr>
      <td>{{$cat->id}}</td>
      <td>{{ $cat->name }}</td>
      <td>{{ $cat->description }}</td>
      <td>
        <div class="btn-group" role="group" aria-label="...">
         <a href="categories/edit/{{ $cat->id }} " class="btn btn-primary"><i class="fa fa-edit fa-fw"></i> Edit</a>
         <a href="categories/delete/{{ $cat->id }} " class="btn btn-danger confirm"><i class="fa fa-trash fa-fw"></i> Delete</a>
         <a href="../category/{{ $cat->name }}" target="_blank" class="btn btn-success"><i class="fa fa-link fa-fw"></i> View</a>
        </div>
      </td>
    </tr>
      @endforeach
  </tbody>
</table>

@endsection