@extends('admin.master')

@section('title') Create New Category @endsection

@section('content')

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>Add New Category</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=" {{ url('/admin/categories') }} ">Categories</a></li>
        <li class="active"><a href=" {{ url('/admin/categories/create') }} ">Add New Category</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<form class="form-horizontal" action="create" method="POST">

{{ csrf_field() }}
	<div class="form-group">
		<label for="title" class="col-sm-2 control-label">Name</label>
		  <div class="col-sm-8">
     	 <input type="text" name="name" class="form-control" id="name" placeholder="Title ...">
    	  </div>
	</div>
	<div class="form-group">
		<label for="desc" class="col-sm-2 control-label">Description</label>
		  <div class="col-sm-8">
		  <textarea class="form-control" id="desc" name="description" placeholder="Text ..."></textarea>

    	  </div>
	</div>
	<div class="form-group">
		  <div class="col-sm-8 col-sm-offset-2">
		  <input type="submit" class="btn btn-primary btn-block" value="Submit" >
    	  </div>
	</div>
</form>

@foreach ($errors->all() as $error)
<ul class="list-unstyled">
<div class="alert alert-danger"><li> {{ $error }} </li></div>
</ul>
@endforeach

@endsection