@extends('admin.master')

@section('title') Admin Panel @endsection



@section('content')


<div class="col-md-6">
  <div class="panel panel-danger">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-3"><i class="fa fa-list-alt fa-fw" style="font-size: 4.5em"></i></div>
        <div class="col-xs-9 text-right">
        <div style="font-size: 2.5em">{{ $posts->count() }}</div>
        <div>Posts</div>
        </div>
      </div>
    </div>
    <a href=" {{ url('admin/posts') }} ">
      <div class="panel-footer">
        <div class="pull-left">View Posts</div>
        <div class="pull-right"><i class="fa fa-arrow-right fa-fw"></i></div>
        <div class="clearfix"></div>
      </div>
    </a>
  </div>
</div>

<div class="col-md-6">
  <div class="panel panel-success">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-3"><i class="fa fa-users fa-fw" style="font-size: 4.5em"></i></div>
        <div class="col-xs-9 text-right">
        <div style="font-size: 2.5em">{{ $users->count() }}</div>
        <div>Users</div>
        </div>
      </div>
    </div>
    <a href="{{ url('admin/users') }}">
      <div class="panel-footer">
        <div class="pull-left">View Users</div>
        <div class="pull-right"><i class="fa fa-arrow-right fa-fw"></i></div>
        <div class="clearfix"></div>
      </div>
    </a>
  </div>
</div>

<div class="col-md-6">
  <div class="panel panel-warning">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-3"><i class="fa fa-comments fa-fw" style="font-size: 4.5em"></i></div>
        <div class="col-xs-9 text-right">
        <div style="font-size: 2.5em">{{ $comments->count() }}</div>
        <div>Comments</div>
        </div>
      </div>
    </div>
    <a href="{{ url('admin/comments') }}">
      <div class="panel-footer">
        <div class="pull-left">View Comments</div>
        <div class="pull-right"><i class="fa fa-arrow-right fa-fw"></i></div>
        <div class="clearfix"></div>
      </div>
    </a>
  </div>
</div>

<div class="col-md-6">
  <div class="panel panel-info">
    <div class="panel-heading">
      <div class="row">
        <div class="col-xs-3"><i class="fa fa-tags fa-fw" style="font-size: 4.5em"></i></div>
        <div class="col-xs-9 text-right">
        <div style="font-size: 2.5em">{{ $cats->count() }}</div>
        <div>Categories</div>
        </div>
      </div>
    </div>
    <a href="{{ url('admin/categories') }}">
      <div class="panel-footer">
        <div class="pull-left">View Categories</div>
        <div class="pull-right"><i class="fa fa-arrow-right fa-fw"></i></div>
        <div class="clearfix"></div>
      </div>
    </a>
  </div>
</div>


<!--
<span></span>
<span>{{ $users->count() }}</span>
<span>{{ $comments->count() }}</span>
<span>{{ $cats->count() }}</span>
-->
 @endsection
