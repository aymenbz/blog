@extends('admin.master')

@section('title') Edit Post @endsection

@section('content')

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>Edit Post</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=" {{ url('/admin/posts') }} ">Posts</a></li>
        <li class="active"><a href=" {{ url('/admin/posts/create') }} ">Edit Post</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<form class="form-horizontal" action="../update/{{ $post->id }}" method="POST" enctype="multipart/form-data">

{{ csrf_field() }}
	<div class="form-group">
		<label id="title" class="col-sm-2 control-label">Title</label>
		  <div class="col-sm-8">
     	 <input type="text" name="title" class="form-control" id="title" value=" {{ $post->title }} ">
    	  </div>
	</div>
	<div class="form-group">
		<label id="body" class="col-sm-2 control-label">Body</label>
		  <div class="col-sm-8">
			<textarea name="body" class="form-control my-editor">{!! $post->body !!}</textarea>

			  </div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Category</label>
		  <div class="col-sm-8">
     	 <select class="form-control" name="category">
     	 <option value="0">Choose Category</option>
				@foreach($categories as $cats)
     	 	<option value="{{$cats->id}}" {{ ($post->category_id == $cats->id) ? 'selected' : null }} >{{$cats->name}}</option>
				@endforeach
			  </select>
    	  </div>
	</div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Tags</label>
      <div class="col-sm-8">
       <select class="form-control select2-multi tags2" name="tags[]" multiple="multiple">
       <option value="0"></option>
       @foreach($tags as $tag)
        <option value="{{$tag->id}}">{{$tag->name}}</option>
        @endforeach
       </select>
        </div>
  </div>
	<div class="form-group">
		<label id="img" class="col-sm-2 control-label">Image</label>

		  <div class="col-sm-8">
		  <input type="file" class="filestyle" data-buttonBefore="true" data-btnClass="btn-primary" value="{{ old('image') }}" name="image">
    	  </div>
    	  <div class="col-sm-3 col-sm-offset-2 img">
	    <img src="{{ url('uploads/images') }}/{{ $post->image }}" width="200px">
	    </div>
	</div>
	<div class="form-group">
		  <div class="col-sm-8 col-sm-offset-2">
		  <input type="submit" class="btn btn-primary btn-block" value="Submit" >
    	  </div>
	</div>
</form>

@foreach ($errors->all() as $error)
<ul class="list-unstyled">
<div class="alert alert-danger"><li> {{ $error }} </li></div>
</ul>
@endforeach

@endsection

@section('footer')
<script type="text/javascript">
$(".tags2").select2({
  tags: true,
})

$(".tags2").select2().val({!! json_encode($post->tags()->allRelatedIds()) !!}).trigger("change");
</script>

@endsection
