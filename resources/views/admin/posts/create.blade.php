@extends('admin.master')

@section('title') Create New Post @endsection

@section('content')

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>Add New Post</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=" {{ url('/admin/posts') }} ">Posts</a></li>
        <li class="active"><a href=" {{ url('/admin/posts/create') }} ">Add New Post</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>
<form class="form-horizontal" action="create" method="POST" enctype="multipart/form-data">

{{ csrf_field() }}
	<div class="form-group">
		<label id="title" class="col-sm-2 control-label">Title</label>
		  <div class="col-sm-8">
     	 <input type="text" name="title" class="form-control" id="title" placeholder="Title ...">
    	  </div>
	</div>
	<div class="form-group">
		<label id="body" class="col-sm-2 control-label">Body</label>
		  <div class="col-sm-8">
			<textarea name="body" class="form-control my-editor">{!! old('content') !!}</textarea>
    	  </div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Category</label>
		  <div class="col-sm-8">
     	 <select class="form-control" name="category">
     	 <option value="0">Choose Category</option>
     	 	@foreach ($cats as $cat)
     	 	<option value="{{$cat->id}}">{{$cat->name}}</option>
     	 	@endforeach
     	 </select>
    	  </div>
	</div>
  <div class="form-group">
		<label class="col-sm-2 control-label">Tags</label>
		  <div class="col-sm-8">
     	 <select class="form-control select2-multi tags" name="tags[]" multiple="multiple">
     	 <option value="0"></option>
     	 	@foreach ($tags as $tag)
     	 	<option value="{{$tag->id}}">{{$tag->name}}</option>
     	 	@endforeach
     	 </select>
    	  </div>
	</div>
	<div class="form-group">
		<label id="img" class="col-sm-2 control-label">Thumbnail</label>
		  <div class="col-sm-8">
			<input type="file" class="filestyle" data-buttonBefore="true" data-btnClass="btn-primary" name="image">
    	  </div>
	</div>


	<div class="form-group">
		  <div class="col-sm-8 col-sm-offset-2">
		  <input type="submit" class="btn btn-primary btn-block" value="Submit" >
    	  </div>
	</div>
</form>

@foreach ($errors->all() as $error)
<ul class="list-unstyled">
<div class="alert alert-danger"><li> {{ $error }} </li></div>
</ul>
@endforeach

@endsection
@section('footer')
<script type="text/javascript">
$(".tags").select2();
</script>
@endsection
