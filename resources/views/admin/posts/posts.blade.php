@extends('admin.master')

@section('title') Posts @endsection

@section('content') 

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>All Posts</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href=" {{ url('/admin/posts') }} ">Posts</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>Title</th>
      <th>Author</th>
      <th>Posted At</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($posts as $post)
    <tr>
      <th>{{$post->id}}</th>
      <th><?php echo substr($post->title, 0, 22) ?> ... </th>
      <th>{{ $post->user->name }}</th>
      <th>{{ $post->created_at }}</th>
      <th>
        <div class="btn-group" role="group" aria-label="...">
         <a href="posts/edit/{{ $post->id }} " class="btn btn-primary"><i class="fa fa-edit fa-fw"></i> Edit</a>
         <a href="posts/delete/{{ $post->id }} " class="btn btn-danger confirm"><i class="fa fa-trash fa-fw"></i> Delete</a>
         <a href="../post/{{ $post->slug }}" target="_blank" class="btn btn-success"><i class="fa fa-link fa-fw"></i> View</a>
        </div>
      </th>
    </tr>
      @endforeach
  </tbody>
</table>
 {{ $posts->links('vendor.pagination.custom') }}
@endsection