@extends('admin.master')

@section('title') Create New Tags @endsection

@section('content')

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>Add New Tag</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=" {{ url('/admin/tags') }} ">Tags</a></li>
        <li class="active"><a href=" {{ url('/admin/categories/create') }} ">Add New Tag</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<form class="form-horizontal" action="create" method="POST">

{{ csrf_field() }}
<div class="form-group">
  <label class="col-sm-2 control-label">Tags</label>
    <div class="col-sm-8">
      <input type="text" name="tags" class="form-control" placeholder="Add Tag">
      </div>
</div>

	<div class="form-group">
		  <div class="col-sm-8 col-sm-offset-2">
		  <input type="submit" class="btn btn-primary btn-block" value="Submit" >
    	  </div>
	</div>
</form>

@foreach ($errors->all() as $error)
<ul class="list-unstyled">
<div class="alert alert-danger"><li> {{ $error }} </li></div>
</ul>
@endforeach

@endsection

@section('footer')

<script type="text/javascript">
$(".tags").select2({
  tags: true,
})

</script>
@endsection
