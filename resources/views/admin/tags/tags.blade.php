@extends('admin.master')

@section('title') Tags @endsection

@section('content')

  <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="col-md-4">
      <h3>All Tags</h3>
      </div>
      <div class="col-md-8 ">
      <ol class="breadcrumb pull-right">
        <li><a href=" {{ url('/admin') }} "><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href=" {{ url('/admin/tags') }} ">Tags</a></li>
      </ol>
      </div>
    </section>

<div style="height: 100px;width: 100px"></div>

<table class="table text-center table-bordered">
  <thead>
    <tr>
      <td>ID</td>
      <td>Name</td>
      <td>Created At</td>
      <td>Action</td>
    </tr>
  </thead>
  <tbody>
  @foreach ($tags as $tag)
    <tr>
      <td>{{$tag->id}}</td>
      <td>{{ $tag->name }}</td>
      <td>{{ $tag->created_at }}</td>
      <td>
        <div class="btn-group" role="group" aria-label="...">
         <a href="tags/edit/{{ $tag->id }} " class="btn btn-primary"><i class="fa fa-edit fa-fw"></i> Edit</a>
         <a href="tags/delete/{{ $tag->id }} " class="btn btn-danger confirm"><i class="fa fa-trash fa-fw"></i> Delete</a>
        </div>
      </td>
    </tr>
      @endforeach
  </tbody>
</table>

@endsection
