@extends('master')

@section('title')  @endsection

@section('content')

@foreach ($posts as $post)
<!-- First Blog Post -->
<article>                
    <img class="img-responsive" src="../uploads/{{ $post->image }}" alt=""/>
        <h2><a href="../post/{{ $post->slug  }} ">{{ $post->title }}</a></h2>
        <p><span class="glyphicon glyphicon-time"></span> Posted on {{ Carbon\Carbon::parse($post->created_at)->format('d-m-Y') }} Category: {{ $post->category->name }}  </p>

        <p> {{ $post->body }} </p>
            <a class="btn btn-primary" href="../post/{{ $post->slug  }} ">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
    <hr>
</article>
@endforeach

                <hr>

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>


@endsection

