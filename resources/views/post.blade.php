@extends('master')

@section('title') {{ $post->title }} @endsection

@section('content')

                <!-- Title -->
                <h1>{{ $post->title }} </h1>
                <hr>

                <!-- Date/Time -->
                <p><i class="fa fa-clock-o"></i> Posted on {{ Carbon\Carbon::parse($post->created_at)->toDayDateTimeString() }} &nbsp; &nbsp;  @if(isset($post->category->name)) <i class="fa fa-tag"></i> Category: {{ $post->category->name }} @endif</p>

                <hr>



                <!-- Post Content -->
                {!! $post->body !!}

                <hr>
                <h4>
                  @foreach ($post->tags as $tag)
                    <span class="label label-default"> {{ $tag->name }} </span>
                  @endforeach
                </h4>
                <!-- Start Author Detail -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src=" {{ url('uploads/images/avatars') }}/{{$user->avatar}} " width="50px" height="50px" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $user->name }}
                            <small> Admin </small>
                        </h4>
                        <p> {{ $user->about }} </p>
                        <small class="social-admin">
                            <ul class="list-unstyled">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-linkedin"></a></li>
                            </ul>
                        </small>
                    </div>
                </div>
                <hr>
                <!-- End Author Detail -->
                <!-- Blog Comments -->

                <!-- Comments Form -->
                @if(Auth::check())
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form action="{{ $post->id }}/store" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                @else
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <p class="lead"><strong><a href=" {{ url('/register') }} ">Register</a></strong> or <strong><a href=" {{ url('/login') }}">Login</a></strong> To Leave A Comment!</p>
                </div>
                @endif
                <hr>

                <!-- Posted Comments -->
                @if($post->comments->count() == 1)
                <h3> {!! $post->comments->count() !!} Comment</h3>
                @elseif ($post->comments->count() > 1)
                <h3> {!! $post->comments->count() !!} Comments</h3>
                @else
                <h4> There Is No Comments!</h4>
                @endif
                <!-- Comment -->
                @foreach($post->comments as $comment)
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src=" {{ url('uploads/images/avatars') }}/{{$comment->user->avatar}} " width="50px" height="50px" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $comment->user->name }}
                            <small> {{ $comment->created_at }} </small>
                        </h4>
                        {{ $comment->body }}
                    </div>
                </div>
                @endforeach


@endsection
