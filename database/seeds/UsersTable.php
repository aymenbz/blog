<?php

use Illuminate\Database\Seeder;

class UsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
         'name' => 'admin',
         'email' => 'admin@gmail.com',
         'password' => bcrypt('123123'),
         'role' => 1,
     ]);

    }
}
