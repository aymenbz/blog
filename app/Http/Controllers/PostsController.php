<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;
use App\User;
use Storage;
use DB;
use App\Tag;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(10);

        return view('admin.posts.posts', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::all();
        $tags = Tag::all();
        return view('admin.posts.create', compact('cats', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [

            'title' => 'required',
            'body' => 'required|min:7',
            'image' => 'image|mimes:png,jpg,jpeg,gif'

            ]);
            $img_name = '';
        if($request->hasFile('image')) {
            $img_name = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'), $img_name);
        } else {
            $img_name = null;
        }

        $post = new Post;
        $post->title = request('title');
        $post->body =  request('body');
        $post->category_id = request('category');
        $post->user_id = auth()->id();
        $post->image = $img_name;
        $post->save();

        $post->tags()->sync(request('tags'), false);

        return redirect('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $post = Post::where('slug', $slug)->first();
        $user = User::first();
        return view('post', compact('post', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate(request(), [

            'title' => 'required|min:7',
            'body' => 'required|min:10',

            ]);

        $post = Post::find($id);
        $post->title = request('title');
        $post->body = request('body');
        $post->category_id = request('category');
        // Add The New Photo
        if($request->hasFile('image')) {
            $img_name = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'), $img_name);
            $oldImgName = $post->image;
        } else {
            $img_name = $post->image;
            $oldImgName = null;
        }
        // Update The Image
        $post->image = $img_name;
        // Delete The Old Image
        Storage::delete($oldImgName);
        $post->save();

        if(request('tags') != null) {
          $post->tags()->sync(request('tags'));
        } else {
          $post->tags()->sync(array());
        }


        return redirect('admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->tags()->detach();
        Storage::delete($post->image);
        $post->delete();
        return redirect('admin/posts');
    }
}
