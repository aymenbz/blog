<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class HomeController extends Controller
{
    
    public function index()
    {

        $posts = Post::latest('id', 'desc')->paginate(5);


        return view('welcome', compact('posts'));
    }
}
