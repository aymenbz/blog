<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagsController extends Controller
{
     public function index()
    {
      $tags = Tag::paginate(10);

      return view('admin.tags.tags', compact('tags'));
    }

    public function create()
    {
      return view('admin.tags.create');
    }

    public function store(Request $request)
    {
      $tags = new Tag;
      $tags->name = request('tags');
      $tags->save();
      return redirect('admin/tags');

    }

    public function edit($id)
    {
    $tags = Tag::find($id);

    return view('admin.tags.edit', compact('tags'));
    }

    public function update(Request $request, $id)
    {
      $this->validate(request(), [

          'name' => 'required|unique:tags,name|min:4',

          ]);

      $tag = Tag::find($id);
      $tag->name = request('name');
      $tag->save();

      return redirect('admin/tags');
    }

    public function destroy($id)
    {
      $tags = Tag::find($id);
      $tags->posts()->detach();
      $tags->delete();
      return redirect('admin/tags');
    }
}
