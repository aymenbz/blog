<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;
use Session;
use DB;

class CategoriesController extends Controller
{


    public function category($name) {

        $category = Category::where('name', $name)->first();

        $posts = $category->posts()->get();

        return view('category', compact('posts'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Il-luminate\Http\Response
     */
    public function index()
    {
        $cats = Category::all();

        return view('admin.categories.categories', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [

            'name' => 'required|unique:categories,name|min:3',
            'description' => 'required|min:7',

            ]);

        Category::create([

            'name' => request('name'),
            'description' =>request('description')

            ]);

        return redirect ('admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = DB::table('categories')->find($id);

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [

            'name' => 'required|unique:categories,name|min:5',
            'description' => 'required|min:7'

            ]);

        Category::where('id', $id)->update($request->except(['_token']));

        return redirect('admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Category::find($id);
        $delete->delete();

        return redirect('admin/categories');
    }
}
