<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Category;
use App\Comment;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
      $posts = Post::all();
      $users = User::all();
      $comments = Comment::all();
      $cats = Category::all();

      return view('admin.index', compact('posts', 'users', 'comments', 'cats'));
    }


}
