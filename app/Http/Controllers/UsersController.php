<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UsersController extends Controller
{
    public function index() {
    	$users = User::where('role', '!=', 1)->paginate(10);

    	return view('admin.users', compact('users'));
    }

    public function destroy($id) {
    	$delete = User::find($id);
    	$delete->delete();

    	return back();
    }
}
