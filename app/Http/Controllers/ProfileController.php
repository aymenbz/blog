<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Storage;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);

        return view('profile', compact('user'));
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [

            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'avatar' => 'image|mimes:jpg,jpeg,png',
        ]);

        $user = User::find($id);
        $user->name = request('name');
        $user->email = request('email');
        // Add The New Photo
        if($request->hasFile('avatar')) {    
            $avatarName = time() . '.' . $request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('uploads/images/avatars'), $avatarName);
            $oldAvatar = $user->avatar;            
        } else {
            $avatarName = $user->avatar;
            $oldImgName = null;
        }
        $user->avatar = $avatarName;
        // Delete The Old Image
        $user->save();

        return redirect('/');

    }

    
}
