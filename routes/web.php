<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/post/{title}', 'PostsController@show');

Route::get('/category/{name}', 'CategoriesController@category');

Route::post('/post/{post}/store', 'CommentsController@store');

Route::get('/profile/{id}', 'ProfileController@index');

Route::post('/profile/{id}/update', 'ProfileController@update');

// Admin Area

Route::group(['middleware' => ['auth', 'admin'] ], function() {

Route::get('/admin', 'AdminController@home');

Route::get('/admin/posts', 'PostsController@index');

Route::get('/admin/posts/create', 'PostsController@create');
Route::post('/admin/posts/create', 'PostsController@store');

// Edit & Update Post

Route::get('/admin/posts/edit/{id}', 'PostsController@edit');
Route::post('/admin/posts/update/{id}', 'PostsController@update');

// Delet Post

Route::get('/admin/posts/delete/{id}', 'PostsController@destroy');

// Create Categories

Route::get('/admin/categories', 'CategoriesController@index');

Route::get('/admin/categories/create', 'CategoriesController@create');
Route::post('/admin/categories/create', 'CategoriesController@store');

// Edit & Update Category

Route::get('/admin/categories/edit/{id}', 'CategoriesController@edit');
Route::post('/admin/categories/update/{id}', 'CategoriesController@update');

// Delete Category

Route::get('/admin/categories/delete/{id}', 'CategoriesController@destroy');

// Comments

Route::get('/admin/comments', 'CommentsController@index');

// Delete Comment

Route::get('/admin/comments/delete/{id}', 'CommentsController@destroy');

// Users

Route::get('/admin/users', 'UsersController@index');

// Delete User

Route::get('/admin/users/delete/{id}', 'UsersController@destroy');

// Tags routes

Route::get('/admin/tags/create', 'TagsController@create');
Route::post('/admin/tags/create', 'TagsController@store');

// Get All tags

Route::get('/admin/tags', 'TagsController@index');

// Edit Tags

Route::get('/admin/tags/edit/{id}', 'TagsController@edit');

// Update Tags

Route::post('/admin/tags/update/{id}', 'TagsController@update');

// Delete Tag

Route::get('/admin/tags/delete/{id}', 'TagsController@destroy');


});
